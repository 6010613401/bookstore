Feature: Manage books
  In order to [goal]
  [stakeholder]
  wants [behaviour]
  
  Scenario: Register new book
    Given I am on the new book page
    When I fill in "Title" with "title 1"
    And I fill in "Author" with "author 1"
    And I fill in "Price" with "120"
    And I press "Save"
    Then I should see "title 1"
    And I should see "author 1"
    And I should see "120"

  Scenario: Delete book
    Given the following books:
      |title|author|price|
      |title 1|author 1|100|
      |title 2|author 2|200|
      |title 3|author 3|300|
      |title 4|author 4|400|
    When I delete the 3rd book
    Then I should see the following books:
      |Title|Author|Price|///
      |title 1|author 1|100.0|Show/Edit/Destroy
      |title 2|author 2|200.0|Show/Edit/Destroy
      |title 4|author 4|400.0|Show/Edit/Destroy
